(function () {
    var CSS = {
        arena: {
            width: 900,
            height: 600,
            background: '#62247B',
            position: 'fixed',
            top: '50%',
            left: '50%',
            zIndex: '999',
            transform: 'translate(-50%, -50%)'
        },
        scoreBoard: {
            width: 300,
            height: 100,
            top: -130,
            left: 300,
            background:'#34c9eb',
            position: 'fixed',
            zIndex: '-1',
            borderStyle: 'solid',
            borderColor: 'black',
            borderWidth: 'medium',
            borderRadius: 20,
        },
        gameModeScreen: {
            width: 900,
            height: 600,
            position: 'fixed',
            top: '50%',
            left: '50%',
            background: '#34c9eb',
            transform: 'translate(-50%, -50%)',

        },
        gameModeElement: {
            width: 300,
            height: 100,
            marginLeft: 300,
            marginTop: 75,
            borderStyle: 'solid',
            borderWidth: 5,
            borderRadius: 20,
            borderColor: 'black',
            fontSize:30,
        },
        loadButton: {
            width: 200,
            height: 75,
            left: 650,
            top: 50,
            position: 'absolute',
            borderStyle: 'solid',
            borderWidth: 5,
            borderRadius: 50,
            borderColor: 'black',
            fontSize:25,

        },
        score1: {
            top: 25,
            left: 50,
            width: 50,
            height: 50,
            position: 'absolute',
            fontSize: 40,
            textAlign: 'center',
            lineHeight: '50px',
        },
        score2: {
            top: 25,
            left: 200,
            width: 50,
            height: 50,
            position: 'absolute',
            fontSize: 40,
            textAlign: 'center',
            lineHeight: '50px',
        },
        ball: {
            width: 15,
            height: 15,
            position: 'absolute',
            top: 0,
            left: 350,
            borderRadius: 50,
            background: '#C6A62F'
        },
        line: {
            width: 0,
            height: 600,
            borderLeft: '2px dashed #C6A62F',
            position: 'absolute',
            top: 0,
            left: '50%'
        },
        stick: {
            width: 12,
            height: 85,
            position: 'absolute',
            background: '#C6A62F'
        },
        stick1: {
            left: 0,
            top: 150
        },
        stick2: {
            right: 0,
            top: 150
        }
    };

    var CONSTS = {
        gameSpeed: 20,
        score1: 0,
        score2: 0,
        stick1Speed: 0,
        stick2Speed: 0,
        ballTopSpeed: 0,
        ballLeftSpeed: 0,
        gameMode: '',
    };

    function start() {
        gameMode();
        setEvents();
        roll();
    }

    function saveData() {
        localStorage.setItem('pong', JSON.stringify(CONSTS));
        localStorage.setItem('ball', JSON.stringify(CSS.ball));

    }

    function loadData() {
        CONSTS = JSON.parse(localStorage.getItem('pong'));
        CSS.ball = JSON.parse(localStorage.getItem('ball'));
    }


    function gameMode() {
        $('<div/>', {id: 'game-mode'}).css(CSS.gameModeScreen).appendTo('body');
        $('<button/>', {id: 'pvp-mode', text: 'Player Versus Player'}).css(CSS.gameModeElement).appendTo('#game-mode');
        $('<button/>', {id: 'pvc-mode', text: 'Player Versus CPU'}).css(CSS.gameModeElement).appendTo('#game-mode');
        $('<button/>', {id: 'cvc-mode', text: 'CPU Versus CPU'}).css(CSS.gameModeElement).appendTo('#game-mode');
        $('<button/>', {id: 'load', text: 'Continue?'}).css(CSS.loadButton).appendTo('#game-mode');
        $("button").click(function () {
            $('#game-mode').remove();
            if (this.id === 'load') {
                loadData();
            }
            CONSTS.gameMode = this.id;
            draw();
            loop();

        });
    }

    function draw() {

        $('<div/>', {id: 'pong-game'}).css(CSS.arena).appendTo('body');
        $('<div/>', {id: 'score-board'}).css(CSS.scoreBoard).appendTo('#pong-game');
        $('<div/>', {id: 'score-1'}).css(CSS.score1).appendTo('#score-board');
        $('<div/>', {id: 'score-2'}).css(CSS.score2).appendTo('#score-board');
        $('<div/>', {id: 'pong-line'}).css(CSS.line).appendTo('#pong-game');
        $('<div/>', {id: 'pong-ball'}).css(CSS.ball).appendTo('#pong-game');
        $('<div/>', {id: 'stick-1'}).css($.extend(CSS.stick1, CSS.stick))
            .appendTo('#pong-game');
        $('<div/>', {id: 'stick-2'}).css($.extend(CSS.stick2, CSS.stick))
            .appendTo('#pong-game');
    }

    function setEvents() {
        $(document).on('keydown', function (e) {
            if (e.keyCode == 87) {
                CONSTS.stick1Speed = -15;
            }
            if (e.keyCode == 83) {
                CONSTS.stick1Speed = +15;
            }
            if (e.keyCode == 38) {
                CONSTS.stick2Speed = -15;
            }
            if (e.keyCode == 40) {
                CONSTS.stick2Speed = +15;
            }
        });

        $(document).on('keyup', function (e) {
            if (e.keyCode == 87) {
                CONSTS.stick1Speed = 0;
            }
            if (e.keyCode == 83) {
                CONSTS.stick1Speed = 0;
            }
            if (e.keyCode == 38) {
                CONSTS.stick2Speed = 0;
            }
            if (e.keyCode == 40) {
                CONSTS.stick2Speed = 0;
            }
            if (e.keyCode == 80) {
                saveData();
            }
        });
    }


    function loop() {
        window.pongLoop = setInterval(function () {

            if (CONSTS.gameMode === 'pvp-mode') {
                CSS.stick1.top += CONSTS.stick1Speed;
                CSS.stick2.top += CONSTS.stick2Speed;
            } else if (CONSTS.gameMode === 'pvc-mode') {
                CSS.stick1.top += CONSTS.stick1Speed;
                if (CSS.stick2.top + CSS.stick.height < CSS.ball.top + CSS.ball.height) {
                    CONSTS.stick2Speed += 2;
                    CSS.stick2.top += CONSTS.stick2Speed;
                } else if (CSS.stick2.top + CSS.stick.height > CSS.ball.top + CSS.ball.height) {
                    CONSTS.stick2Speed -= 2;
                    CSS.stick2.top += CONSTS.stick2Speed;
                }
            } else if (CONSTS.gameMode === 'cvc-mode') {
                if (CSS.stick1.top + CSS.stick.height < CSS.ball.top + CSS.ball.height) {
                    CONSTS.stick1Speed += 2;
                    CSS.stick1.top += CONSTS.stick1Speed;
                } else if (CSS.stick1.top + CSS.stick.height > CSS.ball.top + CSS.ball.height) {
                    CONSTS.stick1Speed -= 2;
                    CSS.stick1.top += CONSTS.stick1Speed;
                }

                if (CSS.stick2.top + CSS.stick.height < CSS.ball.top + CSS.ball.height) {
                    CONSTS.stick2Speed += 2;
                    CSS.stick2.top += CONSTS.stick2Speed;

                } else if (CSS.stick2.top + CSS.stick.height > CSS.ball.top + CSS.ball.height) {
                    CONSTS.stick2Speed -= 2;
                    CSS.stick2.top += CONSTS.stick2Speed;
                }
            }

            if (CSS.stick1.top < 0) {
                CONSTS.stick1Speed = 0;
                CSS.stick1.top = 0
            }
            if (CSS.stick1.top + CSS.stick1.height > CSS.arena.height) {
                CONSTS.stick1peed = 0;
                CSS.stick1.top = CSS.arena.height - CSS.stick1.height;
            }

            if (CSS.stick2.top < 0) {
                CONSTS.stick2Speed = 0;
                CSS.stick2.top = 0;
            }
            if (CSS.stick2.top + CSS.stick2.height > CSS.arena.height) {
                CONSTS.stick2Speed = 0;
                CSS.stick2.top = CSS.arena.height - CSS.stick2.height;
            }


            $('#score-1').text(CONSTS.score1);
            $('#score-2').text(CONSTS.score2);


            CSS.ball.top += CONSTS.ballTopSpeed;
            CSS.ball.left += CONSTS.ballLeftSpeed;


            if (CSS.ball.top <= 0 ||
                CSS.ball.top >= CSS.arena.height - CSS.ball.height) {

                CONSTS.ballTopSpeed = CONSTS.ballTopSpeed * -1;

            }

            $('#pong-ball').css({top: CSS.ball.top, left: CSS.ball.left});

            if (CONSTS.score1 === 5) {
                alert('Player1 win');
                gameOver();

            } else if (CONSTS.score2 === 5) {
                alert('Player2 win');
                gameOver();

            }

            if (CSS.ball.left <= CSS.stick.width) {
                if (CSS.ball.top > CSS.stick1.top && CSS.ball.top < CSS.stick1.top + CSS.stick.height && (CONSTS.ballLeftSpeed = CONSTS.ballLeftSpeed * -1)) {

                    CONSTS.ballLeftSpeed *= 1.1;
                    CONSTS.ballTopSpeed *= 1.1;
                } else {

                    roll();
                    CONSTS.score2 += 1;
                    if (CONSTS.score2 === 5) {
                        $("#pong-ball").remove();
                    }
                }

            }

            if (CSS.ball.left >= CSS.arena.width - CSS.ball.width - CSS.stick.width) {
                if (CSS.ball.top > CSS.stick2.top && CSS.ball.top < CSS.stick2.top + CSS.stick.height && (CONSTS.ballLeftSpeed = CONSTS.ballLeftSpeed * -1)) {

                    CONSTS.ballLeftSpeed *= 1.1;
                    CONSTS.ballTopSpeed *= 1.1;
                } else {

                    roll();
                    CONSTS.score1 += 1;
                    if (CONSTS.score1 === 5) {

                        $("#pong-ball").remove();
                    }
                }
            }
            $('#stick-1').css('top', CSS.stick1.top);
            $('#stick-2').css('top', CSS.stick2.top);

        }, CONSTS.gameSpeed);
    }

    function roll() {
        CSS.ball.top = 250;
        CSS.ball.left = 350;

        var side = -1;

        if (Math.random() < 0.5) {
            side = 1;
        }

        CONSTS.ballTopSpeed = Math.random() * -2 - 3;
        CONSTS.ballLeftSpeed = side * (Math.random() * 2 + 3);
    }

    function gameOver() {
        clearInterval(pongLoop);

        CONSTS.score1 = 0;
        CONSTS.score2 = 0;

        $("#pong-game").remove();
        gameMode();
    }

    start();
})();
